package com.jhonystein.pedidex.model;

import com.jhonystein.pedidex.utils.EnumConverter;
import com.jhonystein.pedidex.utils.EnumType;
import javax.persistence.Converter;

public enum SituacaoPedido implements EnumType {
    
    APROVADO("A"),
    PENDENTE("P");
    
    private final String key;

    private SituacaoPedido(String key) {
        this.key = key;
    }
    
    @Override
    public String getKey() {
        return this.key;
    }
    
    @Converter(autoApply = true)
    public static final class SituacaoPedidoConverter extends EnumConverter<SituacaoPedido> {
        public SituacaoPedidoConverter() {
            super(SituacaoPedido.class);
        }
    }
}
