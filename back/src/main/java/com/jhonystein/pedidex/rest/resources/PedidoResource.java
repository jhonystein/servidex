package com.jhonystein.pedidex.rest.resources;

import com.jhonystein.pedidex.model.Pedido;
import com.jhonystein.pedidex.rest.AbstractCrudResource;
import com.jhonystein.pedidex.services.AbstractCrudService;
import com.jhonystein.pedidex.services.PedidoService;
import com.jhonystein.pedidex.utils.auth.RequestAuth;
import javax.inject.Inject;
import javax.ws.rs.Path;

@RequestAuth
@Path("pedidos")
public class PedidoResource extends AbstractCrudResource<Pedido> {

    @Inject
    private PedidoService service;
    
    @Override
    protected AbstractCrudService<Pedido> getService() {
        return service;
    }
    
}
