import angular from 'angular';

import { default as uiRouter } from '@uirouter/angularjs';
import diretivas from './diretivas';

import { mainConfig } from './main/config';
import { authConfig } from './auth/config';
import { clienteConfig } from './clientes/config';

const modulo = angular
   .module('app', [uiRouter, diretivas]);

export default modulo
  .config(mainConfig)
  .config(authConfig)
  .config(clienteConfig(modulo))
  .constant('BASE_URL', 'http://localhost:8080/api')
  .name;
